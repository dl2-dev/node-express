# Changelog

## [Unreleased]

Not yet released; provisionally 2.0.0 (may change).

## [1.1.0]

### Added

- add all exports from application and exception to root

[1.1.0]: https://bitbucket.org/dl2-dev/node-express/branches/compare/1.1.0..1.0.0
[unreleased]: https://bitbucket.org/dl2-dev/node-express/branches/compare/HEAD..1.1.0
