/* istanbul ignore file */

import { join } from "path";
import { STATUS_DEFAULT } from "../src/exception";
import { Application, noop } from "./support/helpers";

import request = require("supertest");

describe("Application", () => {
  const getRouteFilename = (filename: string): string => {
    return join(__dirname, "support", filename);
  };

  describe("#loadControllerFromFile", () => {
    it("should throw if `actions` or `router` export is missing", () => {
      const error = /module '.+' has no exported member "actions" and.or "route"/;
      const throwable = (filename: string): (() => void) => {
        return () =>
          new Application().loadControllerFromFile(getRouteFilename(filename));
      };

      expect(throwable("errors/missing-actions.ts")).toThrow(error);
      expect(throwable("errors/missing-route.ts")).toThrow(error);
    });

    it("should throw if the given filename is not readable", () => {
      const error = /cannot find module '.+' from '.+'/i;
      const throwable = (filename: string): (() => void) => {
        return () => new Application().loadControllerFromFile(filename);
      };

      expect(throwable("ops.ts")).toThrow(error);
    });

    it("should load only JS and TS files", () => {
      const app = new Application().loadControllerFromFile(
        getRouteFilename("errors/invalid.d.ts"),
      );

      void request(app.express)
        .get("/testing-route")
        .expect(404, () => {
          expect(app).toBeInstanceOf(Application);
        });
    });
  });

  describe("#loadControllerFromPath", () => {
    it("should load all controllers from `routes` directory", (done) => {
      const app = new Application()
        .loadControllerFromPath(getRouteFilename("routes"))
        .mount(noop);

      const $delete = () => {
        void request(app.express).delete("/testing-route").expect(STATUS_DEFAULT, done);
      };

      // not mapped, 404
      const patch = () => {
        void request(app.express).patch("/testing-route").expect(404, $delete);
      };

      // POST "/testing-route" is exported from `routes/subdir/javascript.js`
      const post = () => {
        void request(app.express).post("/testing-route").expect(200, "POST", patch);
      };

      // GET "/testing-route" is exported from `routes/typescript.ts`
      void request(app.express).get("/testing-route").expect(200, "GET", post);
    });
  });
});
