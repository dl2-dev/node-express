import io from "socket.io-client";
import { Application, noop, TMountFn } from "./support/helpers";
import request = require("supertest");

describe("Application", () => {
  let port = 9000;

  const createApp = (): Application => {
    return new Application().withinExpress("set", "port", port);
  };

  beforeEach(() => port++);

  describe("#start", () => {
    it("should start without the `TMountFn` callback parameter", (done) => {
      const mountFn = (app: Application) => {
        app.server.on("listening", () => {
          app.server.close();
          done();
        });
      };

      createApp().mount(mountFn).start();
    });

    it("should run `TMountFn` callback just once", (done) => {
      let count = 0;

      const mountFn: TMountFn = (app: Application) => {
        app.server.on("listening", () => {
          app.server.close();
        });

        if (count++) {
          done(new Error("`TMountFn` called several times"));
        } else {
          done();
        }
      };

      createApp().mount(mountFn).start(mountFn);
    });

    it("should allow 'mount' to be called only once", () => {
      const error = new Error('cannot call "mount(TMountFn)" several times');
      const throwable = () => createApp().mount(noop).mount(noop);

      expect(throwable).toThrow(error);
    });

    it("should wrap within the HTTP server", (done) => {
      const mountFn: TMountFn = (app: Application) => {
        app.server.on("listening", () => app.server.close()).on("ready", done);
      };

      createApp().start(mountFn);
    });

    it("should wrap within the socket.io server", (done) => {
      const mountFn: TMountFn = (app: Application) => {
        app.socketIO?.on("connection", (socket) => {
          socket.on("disconnect", () => void done());
          app.server.close();
        });
      };

      const app = createApp().enableSocketIo().start(mountFn);
      const client = io(`ws://localhost:${port}`);

      void request(app.express)
        .get("/socket.io/socket.io.js")
        .expect(200, () => void client.close());
    });
  });
});
