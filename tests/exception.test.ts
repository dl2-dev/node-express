import { STATUS_CODES } from "http";
import Exception, { STATUS_DEFAULT } from "../src/exception";

describe("Exception", () => {
  it("should have a valid status", () => {
    try {
      throw new Exception(700);
    } catch (err) {
      expect(err).toHaveProperty("status", 500);
      expect(err).toHaveProperty("message", STATUS_CODES[500]);
    }

    const message = STATUS_CODES[STATUS_DEFAULT];

    try {
      throw new Exception(message);
    } catch (err) {
      expect(err).toHaveProperty("status", STATUS_DEFAULT);
      expect(err).toHaveProperty("message", message);
    }
  });

  it("should not initialize `errors` entry automatically", () => {
    const error = { code: "unknown" };

    try {
      throw new Exception().append(error);
    } catch (err) {
      expect(err).toHaveProperty("errors", undefined);
    }

    try {
      throw new Exception({ errors: [] }).append(error).append(error);
    } catch (err) {
      expect(err).toHaveProperty("errors", [error, error]);
    }
  });
});
