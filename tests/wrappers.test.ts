import { Application, noop, TRequestHandler } from "./support/helpers";

import request = require("supertest");

describe("Application", () => {
  describe("#use", () => {
    beforeEach(noop);

    it("should map to Express.use", async () => {
      const handler: TRequestHandler = (req, res) => res.send(header);
      const header = "x-testing-header";
      const middleware: TRequestHandler = (req, res, next) => {
        res.setHeader(header, header);

        next();
      };

      const app = new Application().use(middleware).withinExpress("get", "/", handler);

      await request(app.express).get("/").expect(header, header).expect(200, header);
    });
  });
});
