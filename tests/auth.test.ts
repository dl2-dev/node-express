import { sign, verify } from "jsonwebtoken";
import { Application, noop, TRequestHandler } from "./support/helpers";

import request = require("supertest");

describe("Application", () => {
  describe("#auth", () => {
    const endpoint = "/auth";
    const payload = { username: "testing" };
    const secret = "shhhhh!";
    const token = sign(payload, secret);

    const decodeFn = (token: string): unknown => {
      return verify(token, secret);
    };

    const handler: TRequestHandler = (req, res) => {
      if (req.identity) {
        return res.send((req.identity as { username: string }).username);
      }

      res.send("works");
    };

    const express = (header?: string) => {
      return new Application()
        .enableHeaderAuthorization(decodeFn, header)
        .withinExpress("get", endpoint, handler)
        .mount(noop).express;
    };

    it("should pass to the next middleware if the auth header is empty", (done) => {
      void request(express()).get(endpoint).expect(200, "works", done);
    });

    it("should work with the default authorization header", (done) => {
      void request(express())
        .get(endpoint)
        .set("authorization", token)
        .expect(200, payload.username, done);
    });

    it("should work with custom authorization header", (done) => {
      void request(express("x-authorization"))
        .get(endpoint)
        .set("x-authorization", token)
        .expect(200, payload.username, done);
    });

    // it("should return `400` upon errors during verification", (done) => {
    //   void request(express())
    //     .get(endpoint)
    //     .set("authorization", "ops")
    //     .expect(400, done);
    // });
  });
});
