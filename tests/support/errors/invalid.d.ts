/* eslint-disable @typescript-eslint/ban-ts-comment */
/* istanbul ignore file */
//
// this file should not load
//

import { TRequestHandlerCollection } from "../helpers";

export const route = "/testing-route";

// @ts-ignore
export const actions: TRequestHandlerCollection = {
  get(req, res) {
    res.send("works");
  },
};
