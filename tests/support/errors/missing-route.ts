/* istanbul ignore file */
//
// this file should load as route and throw because it's
// missing the `route` export
//

import { TRequestHandlerCollection } from "../helpers";

export const actions: TRequestHandlerCollection = {
  get(req, res) {
    res.send("works");
  },
};
