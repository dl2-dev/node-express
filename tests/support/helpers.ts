/* istanbul ignore file */
import {
  Application,
  TMountFn,
  TRequestHandler,
  TRequestHandlerCollection,
} from "../../src";

export { Application, TMountFn, TRequestHandler, TRequestHandlerCollection };

export const noop = (): void => {
  return void 0;
};

export const preDispatch: TRequestHandler = (req, res, next) => {
  next();
};
