/* istanbul ignore file */

import { TRequestHandler, TRequestHandlerCollection } from "../helpers";

const beforeGet: TRequestHandler = (req, res, next) => {
  req.identity = req.method;

  next();
};

const get: TRequestHandler = (req, res) => {
  res.send(req.identity);
};

export const route = "/testing-route";
export const actions: TRequestHandlerCollection = {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  delete(req, res) {
    throw new Error();
  },

  get: [beforeGet, get],
};
