import { STATUS_CODES } from "http";

type TError = {
  code?: string;
  field?: string;
  resource?: string;
};

interface IException {
  code?: number | string;
  data?: unknown;
  errors?: TError[];
  message?: string;
  status?: number;
}

export const STATUS_DEFAULT = 400;

export default class Exception extends Error implements IException {
  constructor(error?: string | number | IException) {
    error = error || {};

    switch (typeof error) {
      case "number":
        error = { status: error };
        break;

      case "string":
        error = { message: error };
        break;
    }

    const { code, data, errors } = error;
    const status = statusof(error);

    super(error.message || STATUS_CODES[status]);

    this.code = code;
    this.data = data;
    this.errors = errors;
    this.status = status;
  }

  public append(error: TError): Exception {
    this.errors?.push(error);

    return this;
  }

  public readonly code?: number | string;
  public readonly data?: unknown;
  public readonly errors?: TError[];
  public readonly message!: string;
  public readonly status!: number;
}

function statusof(err: IException): number {
  const { status = STATUS_DEFAULT } = err;

  if (!status || !STATUS_CODES[status]) {
    return 500;
  }

  return status;
}
