/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-var-requires */
import compression = require("compression");
import cors = require("cors");
import express = require("express");
import helmet = require("helmet");

import { join } from "path";
import { Dirent, readdirSync } from "fs";
import { Server } from "http";
import {
  Express,
  NextFunction,
  Params,
  ParamsDictionary,
  Request,
  RequestHandler,
  Response,
} from "express-serve-static-core";
import Exception, { STATUS_DEFAULT } from "./exception";

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      identity?: unknown;
      token?: string;
    }
  }
}

export type TController = {
  actions: TRequestHandlerCollection;
  route: string;
};

export type TDecodeFn = (token: string) => unknown;
export type TMountFn = (app: Application) => void;
export type TNextFn = NextFunction;

export type THandlerType =
  | "all"
  | "checkout"
  | "copy"
  | "delete"
  | "get"
  | "head"
  | "lock"
  | "m-search"
  | "merge"
  | "mkactivity"
  | "mkcol"
  | "move"
  | "notify"
  | "options"
  | "patch"
  | "post"
  | "purge"
  | "put"
  | "report"
  | "search"
  | "subscribe"
  | "trace"
  | "unlock"
  | "unsubscribe"
  | "preDispatch";

// prettier-ignore
export type TRequestHandler<
  P extends Params = ParamsDictionary,
  R = unknown | void
> = (req: Request<P>, res: Response, next: TNextFn) => R | Promise<R>;

// prettier-ignore
export type TRequestHandlerCollection<
  P extends Params = ParamsDictionary,
  R = unknown | void
> = {
  [T in THandlerType]?: TRequestHandler<P, R>[] | TRequestHandler<P, R>;
};

/**
 * Optionated class for `express` applications.
 *
 * Usage:
 * ```js
 * new Application()
 *   .addRoute("foo/bar")
 *   .useDefaultErrorHandlers()
 *   .start();
 * ```
 */
export default class Application {
  public get express(): Express {
    return this.$express;
  }

  public get server(): Server {
    return this.$server;
  }

  public get socketIO(): SocketIO.Server | undefined {
    return this.$socketIO;
  }

  public constructor() {
    this.$express = express()
      .set("jsonp callback name", "cb")
      .set("port", process.env.PORT || 8090)
      .set("trust proxy", 1)
      .use(compression())
      .use(cors())
      .use(express.json({ type: "application/json" }))
      .use(helmet());
  }

  // prettier-ignore
  /**
   * Enable header authentication support.
   *
   * @note this **must** me called before the routes you want to authenticate
   */
  public enableHeaderAuthorization(decodeFn: TDecodeFn, header = "authorization"): Application {
    return this.use((req, res, next) => {
      const token = req.headers[header];

      if (token) {
        req.identity = decodeFn(token as string);
      }

      next();
    });
  }

  /**
   * Enable `socket.io` support.
   */
  public enableSocketIo(): Application {
    this.$socketIOEnabled = true;

    return this;
  }

  public loadControllerFromFile(filename: string): Application {
    const ext = filename.slice(-5);
    if (ext === ".d.ts" || !/\.[jt]s$/.test(ext)) {
      return this;
    }

    const { actions, route }: TController = require(filename);

    if (!actions || !route) {
      throw new Error(
        `module '${filename}' has no exported member "actions" and/or "route"`,
      );
    }

    const { preDispatch = (req, res, next) => void next(), ...handlers } = actions;

    for (const action in handlers) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      this.express[action](route, preDispatch, handlers[action]);
    }

    return this;
  }

  public loadControllerFromPath(dirname: string): Application {
    const files: Dirent[] = readdirSync(dirname, { withFileTypes: true });
    let len = files.length;

    while (len--) {
      const file = files[len];
      const filename = join(dirname, file.name);

      if (file.isDirectory()) {
        this.loadControllerFromPath(filename);
      } else if (filename.endsWith(".ts") || filename.endsWith(".js")) {
        this.loadControllerFromFile(filename);
      }
    }

    return this;
  }

  // prettier-ignore
  /**
   * Starts the HTTP server listening for connections.
   */
  public start(/* istanbul ignore next */ callback: TMountFn = (): void => void 0): Application {
    if (!this.$mounted) {
      this.mount(callback);
    }

    this.$server.listen(this.$express.get("port"));

    return this;
  }

  /**
   * Attach the basic router and get the application ready
   * to listen on `$PORT`.
   */
  public mount(callback: TMountFn): Application {
    if (this.$mounted) {
      throw new Error('cannot call "mount(TMountFn)" several times');
    }

    this.$server = new Server(this.$express);

    if (this.$socketIOEnabled) {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      this.$socketIO = require("socket.io")(this.$server);
    }

    callback(this);

    this.$mounted = true;
    this.$express.emit("ready");
    this.$server.emit("ready");

    this.use(() => {
      throw new Exception(404);
    });

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    this.express.use((err: Exception, req: Request, res: Response, next: TNextFn) => {
      const { code, data, errors, message, status = STATUS_DEFAULT } = err;

      res.status(status).jsonp({
        code,
        data,
        errors,
        message,
      });
    });

    return this;
  }

  /**
   * Alias to `express.use()`.
   */
  public use(...handlers: RequestHandler[]): Application {
    return this.withinExpress("use", ...handlers);
  }

  /**
   * Wrapper for express functions.
   *
   * #### Usage
   * ```ts
   * new Application()
   *   // same as express.on("any event", void 0)
   *   .withinExpress("on", "any event", () => {
   *     console.log("my cool listener. Hello from express!");
   *   })
   *   // same as express.get("/hello", (req, res) => res.jsonp("Hello!"))
   *   .withinExpress("get", "/hello", (req, res) => res.jsonp("Hello!"))
   *   .start();
   * ```
   */
  public withinExpress(method: string, ...args: unknown[]): Application {
    this.$express[method](...args);

    return this;
  }

  protected $express: Express;
  protected $mounted = false;
  protected $server!: Server;
  protected $socketIO?: SocketIO.Server;
  protected $socketIOEnabled = false;
}
